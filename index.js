/**
 * This program is a boliler plate code for the famous tic tac toe game
 * Here box represents one placeholder for either X or a 0
 * We have a 2D array to represent the arrangement of X or O is a grid
 * 0 -> empty box
 * 1 -> box with X
 * 2 -> box with O
 *
 * Below are the tasks which needs to be completed
 * Imagine you are playing with Computer so every alternate move should be by Computer
 * X -> player
 * O -> Computer
 *
 * Winner has to be decided and has to be flashed
 *
 * Extra points will be given for the Creativity
 *
 * Use of Google is not encouraged
 *
 */
let grid = [];
const GRID_LENGTH = 3;
let turn = 'X';

let playerName = 'Anonymous User';

const PLAYER_KEY        = 1;
const COMPUTER_KEY      = 2;
const GAME_OVER         = 'over';
const GAME_TIE          = 'tie';
const GAME_CONTINUE     = 'continue';
const COMPUTER_WON_TEXT = 'Computer Won!';
const GAME_TIED_TEXT    = 'Game Tied';

function initializeGrid() {
    grid = [];

    for (let colIdx = 0;colIdx < GRID_LENGTH; colIdx++) {
        const tempArray = [];
        for (let rowidx = 0; rowidx < GRID_LENGTH;rowidx++) {
            tempArray.push(0);
        }
        grid.push(tempArray);
    }
}

function getRowBoxes(colIdx) {
    let rowDivs = '';
    
    for(let rowIdx=0; rowIdx < GRID_LENGTH ; rowIdx++ ) {
        let additionalClass = 'darkBackground';
        let content = '';
        const sum = colIdx + rowIdx;
        if (sum%2 === 0) {
            additionalClass = 'lightBackground'
        }
        const gridValue = grid[colIdx][rowIdx];
        if(gridValue === 1) {
            content = '<span class="cross">X</span>';
        }
        else if (gridValue === 2) {
            content = '<span class="cross">O</span>';
        }
        rowDivs = rowDivs + '<div colIdx="'+ colIdx +'" rowIdx="' + rowIdx + '" class="box ' +
            additionalClass + '">' + content + '</div>';
    }
    return rowDivs;
}

function getColumns() {
    let columnDivs = '';
    for(let colIdx=0; colIdx < GRID_LENGTH; colIdx++) {
        let coldiv = getRowBoxes(colIdx);
        coldiv = '<div class="rowStyle">' + coldiv + '</div>';
        columnDivs = columnDivs + coldiv;
    }
    return columnDivs;
}

function renderMainGrid() {
    const parent = document.getElementById("grid");
    const columnDivs = getColumns();
    parent.innerHTML = '<div class="columnsStyle">' + columnDivs + '</div>';
}

// Function to check game status.
function checkGameState() {
    let count;
    let countCol;
    let countDiag     = 0;
    let countAntiDiag = 0;
    let totalCount    = 0;

    for (let i = 0; i < GRID_LENGTH; i++) {
        count    = 0;
        countCol = 0;

        for (let j = 0; j < GRID_LENGTH; j++) {

            // Check rows.
            if (grid[i][0] !== 0) {
                if (grid[i][0] === grid[i][j]) {
                    count++;
                }
            }

            // Check columns.
            if (grid[0][i] !== 0) {
                if (grid[0][i] === grid[j][i]) {
                    countCol++;
                }
            }

            // Check diagonal.
            if (grid[0][0] !== 0 && i == j && grid[0][0] === grid[i][j]) {
                countDiag++;
            }

            // Check anit diagonal.
            if (grid[0][GRID_LENGTH-1] !== 0 && i + j === GRID_LENGTH - 1 && grid[0][GRID_LENGTH - 1] === grid[i][j]) {
                countAntiDiag++;
            }

            if (grid[i][j]) {
                totalCount++;
            }

        }

        if (count === GRID_LENGTH) {
            return {
                status: GAME_OVER,
                type: 'row',
                index: i,
                gridValue: grid[i][0]

            };
        }

        if (countCol === GRID_LENGTH) {
            return {
                status: GAME_OVER,
                type: 'col',
                index: i,
                gridValue: grid[0][i]

            };
        }

        if (countDiag === GRID_LENGTH) {
            return {
                status: GAME_OVER,
                type: 'diag',
                index: 0,
                gridValue: grid[0][0]

            };
        }

        if (countAntiDiag === GRID_LENGTH) {
            return {
                status: GAME_OVER,
                type: 'antiDiag',
                index: GRID_LENGTH-1,
                gridValue: grid[0][GRID_LENGTH-1]

            };
        }
    }


    if (totalCount == GRID_LENGTH * GRID_LENGTH) {
        return {
            status: GAME_TIE
        };
    }

    return {
        status: GAME_CONTINUE
    }
}

// Function to check if game is over.
function checkGameOver() {
    let isGameOver = checkGameState();

    if (isGameOver.status === GAME_CONTINUE) {
        return isGameOver;
    }

    let msg = document.getElementById('text');
    msg.style.display = 'block';
    msg.textContent   = COMPUTER_WON_TEXT;

    if (isGameOver.status === GAME_TIE) {
        msg.textContent = GAME_TIED_TEXT;
    } else if (isGameOver.status === GAME_OVER && isGameOver.gridValue === PLAYER_KEY) {
        msg.textContent = playerName + ' won!';
    }

    return isGameOver;
}

// Function to colour winning boxes
function colourWinningBoxes(data) {

    if (data.type === 'row') {
        for (let i = 0; i < GRID_LENGTH; i++ ) {
            let box = document.getElementById(data.index + '' + i);
            box.style.backgroundColor = 'green';
        }
    } else if (data.type === 'col') {
        for (let i = 0; i < GRID_LENGTH; i++ ) {
            let box = document.getElementById(i + '' + data.index);
            box.style.backgroundColor = 'green';
        }

    } else if (data.type === 'diag') {
        for (let i = 0; i < GRID_LENGTH; i++ ) {
            let box = document.getElementById(i + '' + i);
            box.style.backgroundColor = 'green';
        }
    } else if (data.type === 'antiDiag') {
        let j = GRID_LENGTH - 1;
        for (let i = 0; i < GRID_LENGTH; i++ ) {
            let box = document.getElementById(i + '' + j--);
            box.style.backgroundColor = 'green';
        }
    }

}

// Function to run on computer's turn
function computerTurn() {
    let possibleMoves = [];
    let count = 0;

    for (let i = 0; i < GRID_LENGTH; i++) {
        for (let j = 0; j < GRID_LENGTH; j++) {
            if (grid[i][j] === 0) {
                possibleMoves[count++] = {
                    rowIdx: i,
                    colIdx: j

                };
            }
        }
    }

    return possibleMoves[Math.floor((Math.random() * possibleMoves.length))];
}

function onBoxClick() {
    var rowIdx = this.getAttribute("rowIdx");
    var colIdx = this.getAttribute("colIdx");

    if (grid[colIdx][rowIdx] === 0) {
        grid[colIdx][rowIdx] = PLAYER_KEY;

        let checkGame = checkGameOver();

        if (checkGame.status === GAME_OVER || checkGame.status === GAME_TIE) {
            renderMainGrid();
            clickHandlers('remove');

            if (checkGame.status === GAME_OVER) {
                colourWinningBoxes(checkGame);
            }

            return;
        }

        let computerMove = computerTurn();

        if (computerMove) {
            grid[computerMove.rowIdx][computerMove.colIdx] = COMPUTER_KEY;
        }

        checkGame = checkGameOver();

        if (checkGame.status === GAME_OVER || checkGame.status === GAME_TIE) {
            renderMainGrid();
            clickHandlers('remove');

            if (checkGame.status === GAME_OVER) {
                colourWinningBoxes(checkGame);
            }

            return;
        }

        renderMainGrid();
        clickHandlers();
    } else {
        alert('click on empty cell');
    }
}

// Function to add/remove click event on boxes.
function clickHandlers(listener = 'add') {
    const boxes = document.getElementsByClassName("box");

    for (var idx = 0; idx < boxes.length; idx++) {
        if (listener === 'add') {
            boxes[idx].addEventListener('click', onBoxClick, false);
        } else {
            boxes[idx].removeEventListener('click', onBoxClick, false);
        }
    }
}

// Function to get Player Name.
function getPlayerName() {
    name = prompt('Please Enter Your Name', playerName);

    if (name && name !== 'null') {
        playerName = name;
    }
}

// Reset click event to reset the game.
document.getElementById('reset').onclick = function() {
    initializeGrid();
    renderMainGrid();
    clickHandlers();

    let msg = document.getElementById('text');
    msg.style.display = 'none';
};

// Function to start Game by calling all required functions.
function startGame() {
    getPlayerName();
    initializeGrid();
    renderMainGrid();
    clickHandlers();
}

startGame();